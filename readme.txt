﻿Programsko okruženje: PyCharm 2019.2.4
Programski jezik: Python 3.8

Upute za pokretanje:
U programskom okruženju se pokrene glavni program (bez argumenata) smješten u datoteci "main.py" sa Shift+F10 ili na tipku "Run 'main'".
Pokretanjem se izvršavaju svi zadaci iz vježbe.
Rezultat svakog zadatka se ispisuje na ekran i zapisuje u datoteke odgovarajućeg imena (imena datoteka vidljiva su iz glavnog programa).

Napomena:
Na kraju glavnog programa prisutan je zakomentirani dio koda koji testira na nekoliko primjera osnovne matrične operacije koje nisu pokrivene zadacima 1.-10.
Po potrebi se može otkomentirati radi provjere rada.