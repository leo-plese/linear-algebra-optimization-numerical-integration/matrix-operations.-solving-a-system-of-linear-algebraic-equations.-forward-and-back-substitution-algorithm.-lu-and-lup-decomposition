from matrix import Matrix
from copy import deepcopy

def decompose_lu(lu_file_name, A):
    print("LU decomposition...")
    try:
        A.decompose_lu()
        print("A after LU:")
        A.print_matrix()
        A.print_matrix_to_file(lu_file_name)

        return True
    except ValueError as err:
        print(str(err))

        lu_file = open(lu_file_name, "w")
        lu_file.write(str(err))
        lu_file.close()

        return False

def decompose_lup(lup_file_name, A, b=None):
    print("LUP decomposition...")
    try:
        p_vect, n_permutations = A.decompose_lup()
        P = Matrix.create_permutation_matrix(p_vect)

        print("A after LUP:")
        A.print_matrix()
        A.print_matrix_to_file(lup_file_name)

        if b is not None:
            print("b =")
            b = b.reorder_elements(p_vect)
            b.print_matrix()

        print("P = ")
        P.print_matrix()

        return b, n_permutations
    except ValueError as err:
        print(str(err))

        lup_file = open(lup_file_name, "w")
        lup_file.write(str(err))
        lup_file.close()

def solve_equations_system(solution_file_name, A, b):
    try:
        y = A.substitute_forward(b)
        print("y =")
        y.print_matrix()
        x = A.substitute_backward(y)
        print("x =")
        x.print_matrix()
        x.print_matrix_to_file(solution_file_name)
    except ValueError as err:
        print(str(err))

        solution_file = open(solution_file_name, "w")
        solution_file.write(str(err))
        solution_file.close()


if __name__ == "__main__":

    # zad1
    print("Zad 1")
    A1 = Matrix.input_matrix_from_file("zad1_a")
    A1.print_matrix()
    A2 = A1.scale(7)
    A3 = A2.scale(1/7)
    print("A1 = ")
    A1.print_matrix()
    A1.print_matrix_to_file("zad1_a1.txt")
    print("A3 = ")
    A3.print_matrix()
    A1.print_matrix_to_file("zad1_a3.txt")

    is_A13_equal_file = open("zad1_res.txt", "w")
    comp1 = A1 == A3
    print("A1 == A3 (eps = 1e-9): ", comp1)
    is_A13_equal_file.write("A1 == A3 (eps = 1e-9): "+str(comp1) + "\n")
    Matrix.EPS = 1e-30
    comp2 = A1 == A3
    print("A1 == A3 (eps = 1e-30): ", comp2)
    is_A13_equal_file.write("A1 == A3 (eps = 1e-30): " + str(comp2) + "\n")
    Matrix.EPS = 1e-9
    is_A13_equal_file.close()

    try:
        # zad2 - LU ne ide, LUP ide
        print()
        print("Zad 2")
        b_for_lu = Matrix.input_matrix_from_file("zad2_b")      #Matrix(3, 1, [[12], [12], [1]])
        b_for_lup = deepcopy(b_for_lu)
        A_for_lu = Matrix.input_matrix_from_file("zad2_a")      #Matrix(3, 3, [[3,9,6], [4,12,12], [1,-1,1]])
        A_for_lup = deepcopy(A_for_lu)

        print("A =")
        A_for_lu.print_matrix()
        print("b =")
        b_for_lu.print_matrix()

        lu_ok = decompose_lu("zad2_lu.txt", A_for_lu)
        if lu_ok:
            solve_equations_system("zad2_lu_system.txt", A_for_lu, b_for_lu)
        print()
        b_new, _ = decompose_lup("zad2_lup.txt", A_for_lup, b_for_lup)
        if b_new is not None:
            solve_equations_system("zad2_lup_system.txt", A_for_lup, b_new)

        # zad3
        print()
        print("Zad 3")
        b_for_lu = Matrix.input_matrix_from_file("zad3_b")      #Matrix(3, 1, [[12], [12], [1]])
        b_for_lup = deepcopy(b_for_lu)
        A_for_lu = Matrix.input_matrix_from_file("zad3_a")      #Matrix(3, 3, [[1,2,3], [4,5,6], [7,8,9]])   # det = 0 - singularna matrica A! -> Ne moze se rijesiti LU/LUP (uvjet: nesingularne mat <-> mat punog ranga <-> invertibilne mat)
        A_for_lup = deepcopy(A_for_lu)

        print("A =")
        A_for_lu.print_matrix()
        print("b =")
        b_for_lu.print_matrix()

        lu_ok = decompose_lu("zad3_lu.txt", A_for_lu)
        if lu_ok:
            solve_equations_system("zad3_lu_system.txt", A_for_lu, b_for_lu)
        print()
        b_new, _ = decompose_lup("zad3_lup.txt", A_for_lup, b_for_lup)
        #b_new, n_permutations = decompose_lup("zad3_lup.txt", A_for_lup, b_for_lup)
        #print("DET A",A_for_lup.get_determinant(n_permutations))
        if b_new is not None:
            solve_equations_system("zad3_lup_system.txt", A_for_lup, b_new)

        # zad4 - LUP x = (1, 2, 3), LU x = (1.000240, 1.999318, 3.001024) - problem: oduzimanje velikog od malog br.
        # -npr. u 1. iteraciji LU dekomp za element (2. red, 2. stupac) imamo operaciju: 2 * 1e6 - 3 * 1e18 = -2999999999998000128.000000,  a treba biti -2999999999998000000.000000 - RAZLIKA = 128
        print()
        print("Zad 4")
        b_for_lu = Matrix.input_matrix_from_file("zad4_b")      #Matrix(3, 1, [[12000000.000001], [14000000], [10000000]])
        b_for_lup = deepcopy(b_for_lu)
        A_for_lu = Matrix.input_matrix_from_file("zad4_a")      #Matrix(3, 3, [[0.000001,3000000,2000000], [1000000,2000000,3000000], [2000000,1000000,2000000]])
        A_for_lup = deepcopy(A_for_lu)

        print("A =")
        A_for_lu.print_matrix()
        print("b =")
        b_for_lu.print_matrix()

        lu_ok = decompose_lu("zad4_lu.txt", A_for_lu)
        if lu_ok:
            solve_equations_system("zad4_lu_system.txt", A_for_lu, b_for_lu)
        print()
        b_new, _ = decompose_lup("zad4_lup.txt", A_for_lup, b_for_lup)
        if b_new is not None:
            solve_equations_system("zad4_lup_system.txt", A_for_lup, b_new)


        # zad5 - LU ne ide, LUP ide - razlika zbog 1/3 (i visekratnici) ne mogu tocno zapisati u racunalu
        print()
        print("Zad 5")
        b_for_lu = Matrix.input_matrix_from_file("zad5_b")      #Matrix(3, 1, [[6], [9], [3]])
        b_for_lup = deepcopy(b_for_lu)
        A_for_lu = Matrix.input_matrix_from_file("zad5_a")      #Matrix(3, 3, [[0,1,2], [2,0,3], [3,5,1]])
        A_for_lup = deepcopy(A_for_lu)

        print("A =")
        A_for_lu.print_matrix()
        print("b =")
        b_for_lu.print_matrix()

        lu_ok = decompose_lu("zad5_lu.txt", A_for_lu)
        if lu_ok:
            solve_equations_system("zad5_lu_system.txt", A_for_lu, b_for_lu)
        print()
        b_new, _ = decompose_lup("zad5_lup.txt", A_for_lup, b_for_lup)
        if b_new is not None:
            solve_equations_system("zad5_lup_system.txt", A_for_lup, b_new)


        # zad6 (eps = 1e-6) - LU i LUP daju ista rj ali tek u transformiranom sustavu (dolje), a ovdje nema rj. zbog /0 u backward subst!
        # za manji eps - jos manje odstupanje od 0 (eps = 1e-9) mali broj se ne prepoznaje kao 0 jer je abs(x) > eps
        # za eps = 1e-8, 1e-7 ... (i vece) - greska zbog /0 u backward subst!
        print()
        print("Zad 6")
        Matrix.EPS = 1e-6

        b_for_lu = Matrix.input_matrix_from_file("zad6_b")      #Matrix(3, 1, [[9000000000], [15], [0.0000000015]])
        b_for_lup = deepcopy(b_for_lu)
        A_for_lu = Matrix.input_matrix_from_file("zad6_a")      #Matrix(3, 3, [[4000000000,1000000000,3000000000], [4,2,7], [0.0000000003, 0.0000000005, 0.0000000002]])
        A_for_lup = deepcopy(A_for_lu)

        print("A =")
        A_for_lu.print_matrix()
        print("b =")
        b_for_lu.print_matrix()

        lu_ok = decompose_lu("zad6_lu.txt", A_for_lu)
        if lu_ok:
            solve_equations_system("zad6_lu_system.txt", A_for_lu, b_for_lu)
        print()
        b_new, _ = decompose_lup("zad6_lup.txt", A_for_lup, b_for_lup)
        if b_new is not None:
            solve_equations_system("zad6_lup_system.txt", A_for_lup, b_new)

        # transformiran sustav
        print()
        print("transformiran sustav ...")
        b_for_lu = Matrix.input_matrix_from_file("zad6_b_t")        #Matrix(3, 1, [[9], [15], [15]])
        b_for_lup = deepcopy(b_for_lu)
        A_for_lu = Matrix.input_matrix_from_file("zad6_a_t")        #Matrix(3, 3, [[4, 1, 3], [4, 2, 7], [3, 5, 2]])
        A_for_lup = deepcopy(A_for_lu)

        print("A =")
        A_for_lu.print_matrix()
        print("b =")
        b_for_lu.print_matrix()

        lu_ok = decompose_lu("zad6_lu_t.txt", A_for_lu)
        if lu_ok:
            solve_equations_system("zad6_lu_system_t.txt", A_for_lu, b_for_lu)
        print()
        b_new, _ = decompose_lup("zad6_lup_t.txt", A_for_lup, b_for_lup)
        if b_new is not None:
            solve_equations_system("zad6_lup_system_t.txt", A_for_lup, b_new)

        Matrix.EPS = 1e-9

        # zad7 - mat je singularna -> nema inverz!
        print()
        print("Zad 7")
        A = Matrix.input_matrix_from_file("zad7_a")     #Matrix(3, 3, [[1, 2, 3], [4, 5, 6], [7, 8, 9]])
        print("A =")
        A.print_matrix()

        try:
            A_inv = ~A
        except ValueError as err:
            print(str(err))

            zad7_inv_file = open("zad7_inv.txt", "w")
            zad7_inv_file.write(str(err))
            zad7_inv_file.close()
        else:
            print("inv(A) = ")
            A_inv.print_matrix()
            A_inv.print_matrix_to_file("zad7_inv.txt")

        # zad8
        print()
        print("Zad 8")
        A = Matrix.input_matrix_from_file("zad8_a")     #Matrix(3,3,[[4,-5,-2],[5,-6,-2],[-8,9,3]])
        print("A =")
        A.print_matrix()

        try:
            A_inv = ~A
        except ValueError as err:
            print(str(err))

            zad8_inv_file = open("zad8_inv.txt", "w")
            zad8_inv_file.write(str(err))
            zad8_inv_file.close()
        else:
            print("inv(A) = ")
            A_inv.print_matrix()
            A_inv.print_matrix_to_file("zad8_inv.txt")


        # zad9
        print()
        print("Zad 9")
        A = Matrix.input_matrix_from_file("zad9_a")     #Matrix(3, 3, [[4, -5, -2], [5, -6, -2], [-8, 9, 3]])
        print("A =")
        A.print_matrix()

        _, n_permutations = decompose_lup("zad9_lup.txt", A)
        det_A = A.get_determinant(n_permutations)
        print("det(A) =", det_A)
        zad9_det_file = open("zad9_det.txt", "w")
        zad9_det_file.write(str(det_A))
        zad9_det_file.close()


        # zad10
        print()
        print("Zad 10")
        A = Matrix.input_matrix_from_file("zad10_a")        #Matrix(3, 3, [[3, 9, 6], [4, 12, 12], [1, -1, 1]])
        print("A =")
        A.print_matrix()

        _, n_permutations = decompose_lup("zad10_lup.txt", A)
        det_A = A.get_determinant(n_permutations)
        print("det(A) =", det_A)
        zad9_det_file = open("zad10_det.txt", "w")
        zad9_det_file.write(str(det_A))
        zad9_det_file.close()


    except ValueError as decomp_err:
        print(str(decomp_err))



    # SLJEDECE: primjeri osnovnih matricnih operacija iz klase Matrix (par testova za svaku operaciju)
    # print("##############################")
    #
    # mat1 = Matrix(3, 2, [[3, 4], [2, 6], [4, 7.1]])
    # mat2 = Matrix(3, 2, [[-3, 4.5], [11, 44], [33, -2]])
    #
    # print()
    # print("............ Operator +")
    # mat = mat1 + mat2
    # mat.print_matrix()
    # print()
    # mat = mat2 + mat1
    # mat.print_matrix()
    #
    # print()
    # print("............ Operator -")
    # mat = mat1 - mat2
    # mat.print_matrix()
    # print()
    # mat = mat2 - mat1
    # mat.print_matrix()
    # print()
    # mat = mat1 - mat1
    # mat.print_matrix()
    #
    # print()
    # print("............ Matrix multiplication ")
    # mat2 = Matrix(2, 4, [[-3, 4.5, 11, 44], [33, -2, 0, 0.5]])
    # mat3 = Matrix(2, 1, [[-3]])
    # try:
    #     print("mat1 * mat2 =")
    #     mat = mat1 * mat2
    #     mat.print_matrix()
    #
    #     print("mat2 * mat3 =")
    #     mat = mat2 * mat3
    #     mat.print_matrix()
    #
    # except ValueError as err:
    #     print(str(err))
    #
    # print()
    # print("............ Transpose ")
    # mat_t = mat.transpose()
    # mat_t.print_matrix()
    #
    # print()
    # print("............ Operator +=")
    # mat1 = Matrix(3, 2, [[3, 4], [2, 6], [4, 7.1]])
    # mat2 = Matrix(3, 2, [[-3, 4.5], [11, 44], [33, -2]])
    # mat3 = Matrix(2, 1, [[-3], [33]])
    # try:
    #     print("1:")
    #     mat1 += mat2
    #     mat1.print_matrix()
    #     print()
    #     mat2.print_matrix()
    #     print()
    #
    #     print("2:")
    #     mat3 -= mat1
    #     mat1.print_matrix()
    #     print()
    #     mat3.print_matrix()
    #     print()
    # except ValueError as err:
    #     print(str(err))
    #
    #
    # print()
    # print("............ Operator -=")
    # mat1 = Matrix(3, 2, [[3, 4], [2, 6], [4, 7.1]])
    # mat2 = Matrix(3, 2, [[-3, 4.5], [11, 44], [33, -2]])
    # mat3 = Matrix(2, 1, [[-3], [33]])
    # try:
    #     print("1:")
    #     mat1 -= mat2
    #     mat1.print_matrix()
    #     print()
    #     mat2.print_matrix()
    #     print()
    #
    #     print("2:")
    #     mat1 -= mat3
    #     mat1.print_matrix()
    #     print()
    #     mat2.print_matrix()
    #     print()
    # except ValueError as err:
    #     print(str(err))
    #
    #
    # print()
    # print("............ Operator ==")
    # mat1 = Matrix(3, 2, [[3, 4], [2, 6], [4, 7.1]])
    # mat2 = Matrix(3, 2, [[-3, 4.5], [11, 44], [33, -2]])
    # mat3 = Matrix(3, 2, [[-3, 4.5], [11, 44], [33, -2]])
    # mat4 = Matrix(2, 4, [[-3, 4.5, 11, 44], [33, -2, 0, 0.5]])
    # mat5 = Matrix(2, 1, [[-3], [33]])
    # try:
    #     print("mat1 == mat2:", mat1 == mat2)
    #     print("mat1 == mat1:", mat1 == mat1)
    #     print("mat2 == mat2:", mat2 == mat2)
    #     print("mat2 == mat3:", mat2 == mat3)
    #     print("mat3 == mat2:", mat3 == mat2)
    #     print("mat3 == mat4:", mat3 == mat4)
    #     print("mat3 == mat5:", mat3 == mat5)
    # except ValueError as err:
    #     print(str(err))
    #
    #
    # print()
    # print("............ Scaling")
    # mat1 = Matrix(3, 2, [[3, 4], [2, 6], [4, 7.1]])
    # try:
    #     mat1.scale(2.5).print_matrix()
    #     mat1.scale("scale factor").print_matrix()
    # except ValueError as err:
    #     print(str(err))


    print("END")