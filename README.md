Matrix Operations. Solving a system of linear algebraic equations. Forward and Back Substitution Algorithm. LU and LUP Decomposition.

Topics:

- Matrix class

- Forward and Back Substitution

- LU and LUP Decomposition

- Inversion of quadratic matrix

- Determinant of matrix

- Machine precision (single, double)

Implemented in Python.

My lab assignment in Computer Aided Analysis and Design, FER, Zagreb.

Task description in "TaskSpecification.pdf".

Created: 2020

